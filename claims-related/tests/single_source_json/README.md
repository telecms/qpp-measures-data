## Running YoY Comparison for Single Source Json

1. In `single_source_json_comp.py` change `file1` and `file2` to the relevant single source files 
2. Run `python single_source_json_comp.py` from within `/claims-related/tests/single_source_json`
3. Each measure will have its own .md file under `json_report`