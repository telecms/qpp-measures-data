# Comparison for Measure ID 14
Data Element Name Summary
*Added DEN*:set()
*Removed DEN*:set()
*Changed DEN*:{'ENCOUNTER_CODE'}
## For Data Element Name ENCOUNTER_CODE
Codes Summary
* Number of Added CODES*:0
* Number of Removed CODES*:1
* Number of Changed CODES*:0
### Removed Codes
VERSION Measure_ID DATA_ELEMENT_NAME CODING_SYSTEM  CODE    MODIFIER PLACE_OF_SERVICE AGE GENDER
   BASE         14    ENCOUNTER_CODE            C4 99201 ≠GQ, GT, 95              ≠02 ≥50   M, F
