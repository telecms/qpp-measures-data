# Single Source changes Summary
Basefile = 2020_Claims_SingleSource_v1.4.csv
newfile =2021_Claims_SingleSource_v1.3.csv


## Analysis at file level
Base rows only
4560
new rows only
6513
common rows only
14900


## Analysis at Measure ID Level level
*Added Measures*:0
*Removed Measures*:8
*Changed Measures*:48
## Details at Measure ID Level level
*Added Measures*:set()
*Removed Measures*:{'268', '52', '12', '48', '146', '437', '435', '419'}
*Changed Measures*:{'236', '436', '422', '425', '111', '326', '14', '23', '117', '141', '418.01', '181', '182', '195', '47', '254', '317', '112', '24.01', '226.01', '110', '128', '250', '320', '395', '76', '130', '226.02', '93', '416', '21', '261', '155', '418', '1', '24', '147', '50', '225', '249', '226', '406', '134', '39', '145', '154', '113', '405'}
